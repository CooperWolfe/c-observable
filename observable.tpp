#include "observable.h"
#include <map>
#include <vector>

template<class T> unsigned Observable<T>::attach(const std::function<void(T)>& callback) {
  observers.emplace(std::make_pair(next, callback));
  return next++;
}

template<class T> void Observable<T>::detach(const unsigned key) {
  observers.erase(key);
}

template<class T> void Observable<T>::notify(const T& subject) const {
  for (const auto& observer : observers) observer.second(subject);
}
