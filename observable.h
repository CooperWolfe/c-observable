#ifndef OBSERVABLE_H
#define OBSERVABLE_H

#include <functional>
#include <map>

template<class T> class Observable {
public:
  // Default constructor
  Observable(): next(0), observers() {};
  // Adds observer to observer map
  unsigned attach(const std::function<void(T)>& callback);
  // Removes observer from observer map
  void detach(const unsigned key);
  // Notifies observers of subject
  void notify(const T& subject) const;

private:
  unsigned next;
  std::map<unsigned, std::function<void(T)>> observers;
};

#endif // OBSERVABLE_H
